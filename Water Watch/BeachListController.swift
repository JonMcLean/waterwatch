//
//  BeachListController.swift
//  Water Watch
//
//  Created by Jon on 23/10/2015.
//  Copyright © 2015 Jon McLean. All rights reserved.
//

import UIKit
import GoogleMaps

class BeachListController: UIViewController, CLLocationManagerDelegate, GMSMapViewDelegate, SidebarDelegate {
    
    var data: Array<Dictionary<String, AnyObject>> = []
    
    var locationManager: CLLocationManager = CLLocationManager()
    
    var latitude: CLLocationDegrees?
    var longitude: CLLocationDegrees?
    
    // Latitude to send to the BeachInformationController
    var latitudeSend: Double = 0.0
    var longitudeSend: Double = 0.0
    
    // Outlet for the GoogleMaps View.
    @IBOutlet var map: GMSMapView!
    
    var sideBar = Sidebar()
    
    var arrayOfMenuItems = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        // Loading locations along with checking for permissions.
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        
        if CLLocationManager.locationServicesEnabled() {
            if CLLocationManager.authorizationStatus() == .NotDetermined {
                locationManager.requestWhenInUseAuthorization()
            }
        }else {
            let alertController = UIAlertController(title: "Location Services must be enabled", message: "For this app to work properly Location Services must be enabled on your device. To enable Location Services go into Settings > Privacy > Location Services", preferredStyle: .Alert)
            let okAlert = UIAlertAction(title: "Ok", style: .Default, handler: nil)
            
            alertController.addAction(okAlert)
            
            self.presentViewController(alertController, animated: true, completion: nil)
        }
        
        if CLLocationManager.authorizationStatus() == .AuthorizedWhenInUse || CLLocationManager.authorizationStatus() == .AuthorizedAlways {
            print("Authorized")
        }
        
        self.map.delegate = self
        
        latitude = locationManager.location?.coordinate.latitude
        longitude = locationManager.location?.coordinate.longitude
                // Used to fetch data from the GeoNames API, convert it to a dictionary, and then present it.
        let featureFetch = WaterFeatureDataFetcher()
        let json = featureFetch.sendRequestForFeature(latitude!, longitude: longitude!, radius: 300, feature: "BCH&featurecode=BCHS&featurecode=LK&featurecode=STM")
        let jsonConverter = JSON()
        let converted = jsonConverter.convertStringToDictionary(json)
        data = converted!
        
        setupGoogleMaps()
        
        let array: Array<String> = ["Posts", "Spots", "Favourites"]
        arrayOfMenuItems = array
        sideBar = Sidebar(sourceView: self.view, menuItems: array)
        
        self.sideBar.delegate = self
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Menu", style: .Plain, target: self, action: Selector("openSideBar"))
    }
    
    func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        if(status == .AuthorizedWhenInUse) {
            manager.startUpdatingLocation()
        }
    }
    
    func locationManager(manager: CLLocationManager, didUpdateToLocation newLocation: CLLocation, fromLocation oldLocation: CLLocation) {
        self.latitude = newLocation.coordinate.latitude
        self.longitude = newLocation.coordinate.longitude
    }
    
    // Adds Google Maps along with all of the markers to the View Controller.
    func setupGoogleMaps() {
        let mapCamera = GMSCameraPosition.cameraWithLatitude(latitude!, longitude: longitude!, zoom: 8)
        self.map.camera = mapCamera
        
        for i in data {
            let localLong = (i["lng"] as! NSString).doubleValue
            let localLat = (i["lat"] as! NSString).doubleValue
            
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2DMake(localLat, localLong)
            marker.appearAnimation = kGMSMarkerAnimationPop
            marker.map = map
        }
    }
    
    // Used to open another view when Google Maps Marker is pressed.
    func mapView(mapView: GMSMapView!, didTapMarker marker: GMSMarker!) -> Bool {
        latitudeSend = Double(marker.position.latitude)
        longitudeSend = Double(marker.position.longitude)
        
        self.performSegueWithIdentifier("OpenMarkerInformation", sender: self)
        
        return true
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "OpenMarkerInformation") {
            let controller = segue.destinationViewController as! BeachInformationController
            controller.latitude = latitudeSend
            controller.longitude = longitudeSend
        }
    }
    
    // Run whenever sideBar button is pressed.
    func sideBarDidSelectButtonAtIndex(index: Int) {
        if(arrayOfMenuItems[index] as! String == "Posts") {
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("PostNavigationBar") as! UINavigationController
            self.presentViewController(vc, animated: true, completion: nil)
        }else if(arrayOfMenuItems[index] as! String == "Spots") {
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("BeachNavigationBar") as! UINavigationController
            self.presentViewController(vc, animated: true, completion: nil)
        }else if(arrayOfMenuItems[index] as! String == "Favourites") {
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("FavouritesNavigationBar") as! UINavigationController
            self.presentViewController(vc, animated: true, completion: nil)
        }
    }
    
    // Opens sideBar when menu item is pressed.
    func openSideBar() {
        sideBar.showSideBar(!sideBar.isSidebarOpen)
    }
    
}
