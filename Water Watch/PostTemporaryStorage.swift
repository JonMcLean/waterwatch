//
//  PostTemporaryStorage.swift
//  Water Watch
//
//  Created by Jon on 12/10/2015.
//  Copyright © 2015 Jon McLean. All rights reserved.
//

import UIKit

class PostTemporaryStorage {
    
    
    typealias Posts = Array<Dictionary<String, AnyObject>>
    private var ud = NSUserDefaults.standardUserDefaults()
    
    var posts: Posts = []
    
    // Adds the array inputted through parameters to the "posts" variable to be then saved temporarily in NSUserDefaults
    func massAdd(array: Posts) {
        posts = array
        save()
    }
    
    
    // Saves the "posts" array to NSUserDefaults
    func save() {
        ud.setValue(posts, forKey: "posts_temp")
        ud.setBool(true, forKey: "posts_temp_exists")
        ud.synchronize()
    }
    
    // Loads the NSUserDefaults storage into the array object
    func load() {
        if ud.boolForKey("posts_temp_exists") != false {
            posts = ud.valueForKey("posts_temp") as! Posts
        }else {
            print("No cache found when trying to load local posts")
            posts = []
        }
    }
    
}