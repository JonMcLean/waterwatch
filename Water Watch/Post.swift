//
//  Post.swift
//  Water Watch
//
//  Created by Jon McLean on 5/11/2015.
//  Copyright © 2015 Jon McLean. All rights reserved.
//

import UIKit

// Class written by Ben McLean. Slightly edited.
class Post {
    
    var ups = 0
    var downs = 0
    
    var title = ""
    var content = ""
    
    var lat: Double = 0.0
    var lng: Double = 0.0
    
    var date: String = ""
    
    var postID = ""
    var userID = ""
    
    init(ups: Int, downs: Int, title: String, content: String, lat: Double, lng: Double, dateString: String, postID: String, userID: String) {
        self.ups = ups
        self.downs = downs
        self.title = title
        self.content = content
        self.lat = lat
        self.lng = lng
        self.date = dateString
        self.postID = postID
        self.userID = userID
    }
    
    init() { }
    
}
