//
//  BeachInformationController.swift
//  Water Watch
//
//  Created by Jon on 3/11/2015.
//  Copyright © 2015 Jon McLean. All rights reserved.
//

import UIKit
import CoreData

class BeachInformationController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    var beachData: Dictionary<String, AnyObject>?
    
    var postsTemp = PostTemporaryStorage()
    
    var indexSend = 0
    
    @IBOutlet var informationTextView: UITextView!
    
    @IBOutlet var postsTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        postsTableView.delegate = self
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        postsTemp.load()
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "More", style: .Plain, target: self, action: Selector("moreAction"))
        self.fillInformation()
        
    }
    
    // Gathers inforamtion on the beach and then shows the necessary information for the API to the user.
    func fillInformation() {
        let featureFetch = WaterFeatureDataFetcher()
        let jsonString = featureFetch.sendRequestForFeature(latitude, longitude: longitude, radius: 1, feature: "BCH&featurecode=BCHS&featurecode=LK&featurecode=STM")
        
        let jsonConverter = JSON()
        let converted = jsonConverter.convertStringToDictionary(jsonString)
        
        informationTextView.text = "Name: \(converted![0]["name"] as! String) \nCountry: \(converted![0]["countryName"] as! String) \nState: \(converted![0]["adminName1"] as! String) \nLatitude: \(converted![0]["lat"] as! String) \nLongitude: \(converted![0]["lng"] as! String)"
        
        loadPosts(15)
        
        beachData = converted![0]
    }
    
    // Used to add spots to your favourites list. It also later will have the functionality to share your favourite spots with your friends.
    func moreAction() {
        
        let moreActionSheet = UIAlertController(title: "More...", message: "", preferredStyle: .ActionSheet)
        
        let addFavourite = UIAlertAction(title: "Add to Favourites", style: .Default) { (action: UIAlertAction) -> Void in
            let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
            let managedContext = appDelegate.managedObjectContext
            let entity = NSEntityDescription.entityForName("FavouriteBeaches", inManagedObjectContext: managedContext)
            let beach = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: managedContext)
            
            let beachName = self.beachData!["name"] as! String
            
            beach.setValue(beachName, forKey: "name")
            beach.setValue(self.latitude, forKey: "latitude")
            beach.setValue(self.longitude, forKey: "longitude")
            
            do {
                try managedContext.save()
            }catch let error as NSError {
                print("Error while trying to save data to CoreData (BeachInformationController:moreAction:addFavourite): \(error.localizedDescription)")
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Default, handler: nil)
        
        moreActionSheet.addAction(addFavourite)
        moreActionSheet.addAction(cancelAction)
        
        self.presentViewController(moreActionSheet, animated: true, completion: nil)
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("PostsCell", forIndexPath: indexPath)
        
        let dictionary = postsTemp.posts[indexPath.row]
        
        let postName = dictionary["name"] as! String
        let postVoteCount = dictionary["voteCount"] as! Int
        
        cell.textLabel!.text = postName
        cell.detailTextLabel!.text = String(postVoteCount)
        
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(postsTemp.posts.count > 3) {
            return postsTemp.posts.count - (postsTemp.posts.count - 3)
        }else {
            return postsTemp.posts.count
        }
    }
    
    func tableView(tableView: UITableView, accessoryButtonTappedForRowWithIndexPath indexPath: NSIndexPath) {
        self.moveToPost(indexPath)
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.moveToPost(indexPath)
    }
    
    // Opens the PostDetaiViewController
    func moveToPost(indexPath: NSIndexPath) {
        self.postsTableView.deselectRowAtIndexPath(indexPath, animated: true)
        self.indexSend = indexPath.row
        self.performSegueWithIdentifier("OpenPostDetail", sender: self)
    }
    
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "OpenPostDetail") {
            let controller = segue.destinationViewController as! PostDetailDisplayController
            controller.index = indexSend
        }
    }
    
    
    // Loads a certain number of posts according to the offset.
    func loadPosts(offset: Int) {
        let posts = API.getMessages(latitude, lng: longitude, offset: offset, sort: API.Sort.hot)
        
        var array: Array<Dictionary<String, AnyObject>> = []
        
        for i in posts {
            let dictionary: Dictionary<String, AnyObject> = ["name": i.title, "voteCount": (i.ups - i.downs), "latitude": i.lat, "longitude": i.lng, "content": i.content, "postID": i.postID, "userID": i.userID]
            array.append(dictionary)
        }
        
        postsTemp.massAdd(array)
        postsTemp.save()
    }
    
    
}
