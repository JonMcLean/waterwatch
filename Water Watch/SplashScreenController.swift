//
//  LoadingScreenViewController.swift
//  Water Watch
//
//  Created by Jon on 12/10/2015.
//  Copyright © 2015 Jon McLean. All rights reserved.
//

import UIKit
import CoreLocation

class SplashScreenController: UIViewController, CLLocationManagerDelegate {
    
    typealias Posts = Array<Dictionary<String, AnyObject>>
    
    @IBOutlet var logoImage: UIImageView!
    @IBOutlet var spinner: UIActivityIndicatorView!
    
    private var postStorage = PostTemporaryStorage()
    
    private let locationManager = CLLocationManager()
    
    private var latitude: Double = 0.0
    private var longitude: Double = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // Used to load everything that is required for the app to begin working.
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        setupView()
        spinner.startAnimating()
        postStorage.load()
        
        locationManager.requestWhenInUseAuthorization()
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
        
        postStorage.save()
        
        spinner.stopAnimating()
        self.performSegueWithIdentifier("OpenMainView", sender: self)
    }
    
    func locationManager(manager: CLLocationManager, didUpdateToLocation newLocation: CLLocation, fromLocation oldLocation: CLLocation) {
        latitude = Double(newLocation.coordinate.latitude)
        longitude = Double(newLocation.coordinate.longitude)
    }
    
    // Setups the activity spinner and logo image.
    func setupView() {
        spinner.hidesWhenStopped = true
        logoImage.image = UIImage(named: "water_watch_logo") // just to be sure.
    }
    
    // Loads posts from server and inserts them into the temporary post storage.
    func loadPosts(offset: Int) {
        let posts = API.getMessages(latitude, lng: longitude, offset: offset, sort: API.Sort.hot)
        
        var array: Posts = []
        
        for i in posts {
            let dictionary: Dictionary<String, AnyObject> = ["name": i.title, "voteCount": (i.ups - i.downs), "latitude": i.lat, "longitude": i.lng, "content": i.content, "postID": i.postID, "userID": i.userID]
            array.append(dictionary)
        }
        
        postStorage.massAdd(array)
        postStorage.save()
    }
    
}