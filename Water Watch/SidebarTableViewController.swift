//
//  SideBarTableViewController.swift
//  Water Watch
//
//  Created by Jon on 23/10/2015.
//  Copyright © 2015 Jon McLean. All rights reserved.
//

import UIKit

protocol SideBarTableViewControllerDelegate {
    func sideBarControlDidSelectRow(indexPath: NSIndexPath)
}

class SideBarTableViewController: UITableViewController {
    
    var delegate: SideBarTableViewControllerDelegate?
    var tableData: Array<String> = []
    
    // Sets the number of sections to 1 as we do not need anymore than one section in the UITableView.
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
    // Gives the table the number of rows in section
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableData.count
    }
    
    
    // Defines the structure and content for all of the cells as they are being written.
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var cell: UITableViewCell? = tableView.dequeueReusableCellWithIdentifier("MenuCell")
        
        if cell == nil {
            cell = UITableViewCell(style: .Default, reuseIdentifier: "MenuCell")
            cell!.backgroundColor = UIColor.clearColor()
            cell!.textLabel!.textColor = UIColor.darkTextColor()
            
            let selectedView = UIView(frame: CGRect(x: 0, y: 0, width: cell!.frame.size.width, height: cell!.frame.size.height))
            cell!.selectedBackgroundView = selectedView
            
            cell!.textLabel!.text = tableData[indexPath.row]
        }
        
        return cell!
    }
    
    // Sets the height for a row/cell within the tableView.
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 45.0
    }
    
    // Runs whenever a cell is clicked.
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        delegate?.sideBarControlDidSelectRow(indexPath)
    }
    
}
