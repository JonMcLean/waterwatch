//
//  Structures.swift
//  Water Watch
//
//  Created by Jon on 2/11/2015.
//  Copyright © 2015 Jon McLean. All rights reserved.
//

import UIKit

// Structure simply designed to make storing latitude and longitude more simple
struct Location {
    var latitude: Double
    var longitude: Double
}
