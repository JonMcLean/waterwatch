//
//  Sidebar.swift
//  Water Watch
//
//  Created by Jon on 1/11/2015.
//  Copyright © 2015 Jon McLean. All rights reserved.
//

import UIKit


@objc protocol SidebarDelegate { // The @objc is required for the optional keyword.
    func sideBarDidSelectButtonAtIndex(index: Int)
    optional func sideBarWillAppear()
    optional func sideBarWillDisappear()
}

class Sidebar: NSObject, SideBarTableViewControllerDelegate {
    
    // General Variables
    let barWidth: CGFloat = 150.0
    let sideBarTableTopInset: CGFloat = 64.0
    let sideBarContainer = UIView()
    let sideBarTableViewController = SideBarTableViewController()
    
    var originView = UIView()
    var animator: UIDynamicAnimator = UIDynamicAnimator()
    var delegate: SidebarDelegate?
    var isSidebarOpen: Bool = false
    
    
    // Empty init. This is used just for allocating memory for the object without defining parameters.
    override init() {
        super.init()
    }
    
    
    // Init that requires the parameters of a sourceView and an array of all of the sideBar buttons
    init(sourceView: UIView, menuItems: Array<String>) {
        super.init()
        originView = sourceView
        sideBarTableViewController.tableData = menuItems
        sideBarTableViewController.delegate = self
        
        setupSidebar()
        
        animator = UIDynamicAnimator(referenceView: originView)
        
        // Gestures used for showing and hiding the Sidebar.
        let showGestureRecogniser = UISwipeGestureRecognizer(target: self, action: Selector("handleShowSwipe"))
        showGestureRecogniser.direction = UISwipeGestureRecognizerDirection.Right
        originView.addGestureRecognizer(showGestureRecogniser)
        
        let hideGestureRecognizer = UISwipeGestureRecognizer(target: self, action: Selector("handleHideSwipe"))
        hideGestureRecognizer.direction = UISwipeGestureRecognizerDirection.Left
        originView.addGestureRecognizer(hideGestureRecognizer)
    }
    
    // Sets up the sideBar.
    func setupSidebar() {
        sideBarContainer.frame = CGRect(x: -barWidth - 1, y: originView.frame.origin.y, width: barWidth, height: originView.frame.size.height)
        sideBarContainer.clipsToBounds = false
        
        // Sets the background image.
        var backgroundImage = UIImage(named: "menu_lake_background")!
        sideBarContainer.backgroundColor = UIColor(patternImage: UIImage(named: "menu_lake_background")!)
        
        let blurEffect = UIBlurEffect(style: .Dark)
        let blurView = UIVisualEffectView(effect: blurEffect)
        blurView.frame = sideBarContainer.frame
        blurView.autoresizingMask = [UIViewAutoresizing.FlexibleWidth, UIViewAutoresizing.FlexibleHeight]
        blurView.translatesAutoresizingMaskIntoConstraints = true
        
        sideBarContainer.addSubview(blurView)
        
        originView.addSubview(sideBarContainer)
        
        // Defining necessary parameters for the tableView.
        sideBarTableViewController.delegate = self
        sideBarTableViewController.tableView.frame = sideBarContainer.bounds
        sideBarTableViewController.tableView.clipsToBounds = false
        sideBarTableViewController.tableView.separatorStyle = .None
        sideBarTableViewController.tableView.backgroundColor = UIColor.clearColor()
        sideBarTableViewController.tableView.scrollsToTop = false
        sideBarTableViewController.tableView.contentInset = UIEdgeInsetsMake(sideBarTableTopInset, 0, 0, 0)
        
        sideBarTableViewController.tableView.reloadData()
        
        let vibrancyEffect = UIVibrancyEffect(forBlurEffect: blurEffect)
        let vibrancyView = UIVisualEffectView(effect: vibrancyEffect)
        sideBarTableViewController.tableView.addSubview(vibrancyView)
        
        sideBarContainer.addSubview(sideBarTableViewController.tableView)
    }
    
    // Run when show gesture is performed.
    func handleShowSwipe() {
        showSideBar(true)
        delegate?.sideBarWillAppear?()
    }
    
    // Run when hide gesture is performed.
    func handleHideSwipe() {
        showSideBar(false)
        delegate?.sideBarWillDisappear?()
    }
    
    // Responsible for showing the sideBar and preparing the view.
    func showSideBar(shouldOpen: Bool) {
        animator.removeAllBehaviors()
        isSidebarOpen = shouldOpen
        
        if shouldOpen == false {
            print("Should be closing")
        }else {
            print("Should be opening")
        }
        
        let gravityX: CGFloat = (shouldOpen) ? 0.5 : -0.5
        let magnitude: CGFloat = (shouldOpen) ? 20.0 : -20.0
        let boundary: CGFloat = (shouldOpen) ? barWidth : -barWidth - 1
        
        // GravityBebaviour, CollisionBehaviour, and PushBehaviour all helped to create the bounce effect at the end of the movement of the sideBar.
        let gravityBehaviour: UIGravityBehavior = UIGravityBehavior(items: [sideBarContainer])
        gravityBehaviour.gravityDirection = CGVectorMake(gravityX, 0)
        animator.addBehavior(gravityBehaviour)
        
        let collisionBehavior = UICollisionBehavior(items: [sideBarContainer])
        collisionBehavior.addBoundaryWithIdentifier("sideBarBoundary", fromPoint: CGPointMake(boundary, 20), toPoint: CGPointMake(boundary, originView.frame.size.height))
        animator.addBehavior(collisionBehavior)
        
        let pushBehavior = UIPushBehavior(items: [sideBarContainer], mode: UIPushBehaviorMode.Instantaneous)
        pushBehavior.magnitude = magnitude
        
        animator.addBehavior(pushBehavior)
        
        let sideBarBehavior = UIDynamicItemBehavior(items: [sideBarContainer])
        sideBarBehavior.elasticity = 0.3
        
        animator.addBehavior(sideBarBehavior)
    }
    
    func sideBarControlDidSelectRow(indexPath: NSIndexPath) {
        delegate?.sideBarDidSelectButtonAtIndex(indexPath.row)
    }
    
}
