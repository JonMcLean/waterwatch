//
//  FavouriteDisplayController.swift
//  Water Watch
//
//  Created by Jon on 2/11/2015.
//  Copyright © 2015 Jon McLean. All rights reserved.
//

import UIKit
import CoreData

class FavouritesListController: UIViewController, UITableViewDataSource, UITableViewDelegate, SidebarDelegate{
    
    var favouritesManaged: Array<NSManagedObject> = []
    
    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    
    @IBOutlet var tableView: UITableView!
    
    var postIDSend = ""
    
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    
    var arrayOfMenuItems = []
    
    var sideBar = Sidebar()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // This area is generally responsible for loading the data from CoreData.
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        let managedContext = appDelegate.managedObjectContext
        let fetchRequest = NSFetchRequest(entityName: "FavouriteBeaches")
        do {
            let results = try managedContext.executeFetchRequest(fetchRequest)
            favouritesManaged = results as! [NSManagedObject]
        }catch let error as NSError {
            print("Error occured while fetching Favourites from the CoreData model (FavouritesListController:viewWillAppear): \(error.localizedDescription)")
        }
        
        // Defines parameters for Sidebar.
        let array: Array<String> = ["Posts", "Spots", "Favourites"]
        arrayOfMenuItems = array
        sideBar = Sidebar(sourceView: self.view, menuItems: array)
        
        self.sideBar.delegate = self
        
        self.tableView.delegate = self
        self.tableView.dataSource = self
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(favouritesManaged.count)
        return favouritesManaged.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("FavouriteSpotCell", forIndexPath: indexPath)
        
        let favouriteObject = favouritesManaged[indexPath.row]
        let name = favouriteObject.valueForKey("name") as! String
        
        cell.textLabel!.text = name
        
        return cell
    }
    
    // Allows cells to be deleted whenever the users wants to.
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if editingStyle == UITableViewCellEditingStyle.Delete {
            
            let managedContext = appDelegate.managedObjectContext
            let objectToDelete = favouritesManaged[indexPath.row] as! NSManagedObject
            managedContext.deleteObject(objectToDelete)
            favouritesManaged.removeAtIndex(indexPath.row)
            
            do {
                try managedContext.save()
            }catch let error as NSError {
                print("Error while trying to save changed (ViewController:tableView commitEditingStyle): \(error.localizedDescription)")
            }
            
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Automatic)
        }
    }
    
    func tableView(tableView: UITableView, accessoryButtonTappedForRowWithIndexPath indexPath: NSIndexPath) {
        let object = favouritesManaged[indexPath.row]
        latitude = object.valueForKey("latitude") as! Double
        longitude = object.valueForKey("longitude") as! Double
        
        handleCellSelection(indexPath)
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let object = favouritesManaged[indexPath.row]
        latitude = object.valueForKey("latitude") as! Double
        longitude = object.valueForKey("longitude") as! Double
        handleCellSelection(indexPath)
    }
    
    func handleCellSelection(indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("OpenBeachInformationForFavourite", sender: self)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "OpenBeachInformationForFavourite") {
            let vc = segue.destinationViewController as! BeachInformationController
            vc.latitude = self.latitude
            vc.longitude = self.longitude
        }
    }
    
    // Run whenever sideBar button is pressed.
    func sideBarDidSelectButtonAtIndex(index: Int) {
        if(arrayOfMenuItems[index] as! String == "Posts") {
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("PostNavigationBar") as! UINavigationController
            self.presentViewController(vc, animated: true, completion: nil)
        }else if(arrayOfMenuItems[index] as! String == "Spots") {
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("BeachNavigationBar") as! UINavigationController
            self.presentViewController(vc, animated: true, completion: nil)
        }else if(arrayOfMenuItems[index] as! String == "Favourites") {
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("FavouritesNavigationBar") as! UINavigationController
            self.presentViewController(vc, animated: true, completion: nil)
        }
    }
    
}
