//
//  PostCreationController.swift
//  Water Watch
//
//  Created by Jon on 17/10/2015.
//  Copyright © 2015 Jon McLean. All rights reserved.
//

import UIKit
import CoreLocation

class PostCreationController: UIViewController, UITextViewDelegate, CLLocationManagerDelegate {
    
    // Outlets of some elements of the View Controller
    @IBOutlet var titleField: UITextField!
    @IBOutlet var contentTextView: UITextView!
    @IBOutlet var charactersRemaining: UILabel!
    
    var latitude = 0.0
    var longitude = 0.0
    
    let locationManager = CLLocationManager()
    
    let postID = NSUUID().UUIDString
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        contentTextView.text = ""
        contentTextView.delegate = self
        contentTextView.layer.borderWidth = 1.0
        contentTextView.layer.borderColor = UIColor.lightGrayColor().CGColor
        
        // Gesture recogniser used detect when a user taps outside the keyboard area and therefore close the keyboard.
        let closeKeyboardTap = UITapGestureRecognizer(target: self, action: Selector("closeKeyboard"))
        self.view.addGestureRecognizer(closeKeyboardTap)
        
        // Adds a BarButtonItem to the right bar button item spot. This button is then used to send all of the data to the server.
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Create", style: .Plain, target: self, action: Selector("createButtonPressed"))
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.startUpdatingLocation()
    }
    
    func locationManager(manager: CLLocationManager, didUpdateToLocation newLocation: CLLocation, fromLocation oldLocation: CLLocation) {
        latitude = Double(newLocation.coordinate.latitude)
        longitude = Double(newLocation.coordinate.longitude)
    }
    
    // Used to update the character count
    func textViewDidChange(textView: UITextView) {
        let content = textView.text
        let count = content.characters.count
        let remaining = 1000 - count
        charactersRemaining.text = String(remaining) + " characters remaining"
    }
    
    // Closes the keyboard
    func closeKeyboard() {
        view.endEditing(true)
    }
    
    func createButtonPressed() {
        let postThread = NSThread(target: self, selector: Selector("postThread"), object: nil)
        postThread.start()
        self.performSegueWithIdentifier("BackToDisplay", sender: self)
    }
    
    // Gets user ID.
    func getUserID() -> String {
        let id = Identification()
        let actualId = id.getIdentifier()
        return actualId
    }
    
    // Function run of seperate thread tasked with posting a post to the server.
    func postThread() {
        API.postMessage(titleField.text!, message: contentTextView.text, lat: latitude, lng: longitude, postID: postID, userID: self.getUserID())
    }
}
