//
//  PostNetworking.swift
//  Water Watch
//
//  Created by Jon on 17/10/2015.
//  Copyright © 2015 Jon McLean. All rights reserved.
//

import UIKit

class Networking {
    
    // Sends request to url and returns the result.
    class func getResult(urlPath: String) -> AnyObject? {
        // Creating a NSURL using the function parameter.
        let url = NSURL(string: urlPath)
        
        // Creates an NSURLRequest based upon the NSURL.
        let request = NSURLRequest(URL: url!)
        
        // Creates a nil variable with the type AutoReleasingUnsafeMutablePointer<NSURLResponse?> which will be used later to get the returning http status reponse.
        let response: AutoreleasingUnsafeMutablePointer<NSURLResponse?> = nil
        
        // Sends the synchronous request and stores the returned NSData.
        let dataVal: NSData = try! NSURLConnection.sendSynchronousRequest(request, returningResponse: response)
        
        // Converts JSON to AnyObject
        let jsonResult: AnyObject? = (try? NSJSONSerialization.JSONObjectWithData(dataVal, options: NSJSONReadingOptions.MutableContainers))
        
        return jsonResult
    }
    
    // Sends an asynchrnous request to the server
    class func asyncRequest(urlPath: String) {
        
        // Creates NSURL from urlPath function parameter.
        let url = NSURL(string: urlPath)!
        
        // Creates a NSURLRequest based upon earlier said NSURL
        let request = NSURLRequest(URL: url)
        
        // Creates the operation queue.
        let queue = NSOperationQueue()
        
        // Sends the Asyncrhnous request.
        NSURLConnection.sendAsynchronousRequest(request, queue: queue, completionHandler: { response, data, error in})
        
    }
}

