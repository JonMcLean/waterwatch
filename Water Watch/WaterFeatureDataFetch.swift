//
//  WaterFeatureDataFetcher.swift
//  Water Watch
//
//  Created by Jon on 21/10/2015.
//  Copyright © 2015 Jon McLean. All rights reserved.
//

import Foundation

class WaterFeatureDataFetcher {
    
    // Sends request for a certain type or certain types of features to the GeoNames API. This will then return a JSON String to be converted into an array.
    func sendRequestForFeature(latitude: Double, longitude: Double, radius: Int, feature: String) -> String {
        let stringURL = "http://api.geonames.org/findNearbyJSON?lat=\(String(latitude))&lng=\(String(longitude))&featureCode=\(feature)&radius=\(String(radius))&style=FULL&maxRows=25&username=edhe"
        let URL = NSURL(string: stringURL)!
        let request = NSMutableURLRequest(URL: URL)
        request.HTTPMethod = "GET"
        
        var returnString: NSString = ""
        do {
            let returnData = try NSURLConnection.sendSynchronousRequest(request, returningResponse: nil) // deprecated
            returnString = NSString(data: returnData, encoding: NSUTF8StringEncoding)!
        }catch let error as NSError{
            print("Error while getting JSON (WaterFeatureDataFetch:sendRequestForFeature): \(error.localizedDescription)")
        }
        return returnString as String
    }
    
    // Sends a general request for the closest anything to the inputted location. This then, like the other function, returns a JSON String to be
    // converted into an array
    func sendGeneralRequest(latitude: Double, longitude: Double) -> String{
        let stringURL = "http://api.geonames.org/findNearbyJSON?lat=\(latitude)&lng=\(longitude)&username=edhe"
        let URL = NSURL(string: stringURL)!
        let request = NSMutableURLRequest(URL: URL)
        request.HTTPMethod = "GET"
        
        var returnString: NSString = ""
        do {
            let returnData = try NSURLConnection.sendSynchronousRequest(request, returningResponse: nil)
            returnString = NSString(data: returnData, encoding: NSUTF8StringEncoding)!
        }catch let error as NSError {
            print("Error while getting JSON (WaterFeatureDataFetch:sendGeneralRequest): \(error.localizedDescription)")
        }
        return returnString as String
    }
    
}
