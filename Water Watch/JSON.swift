//
//  JSON.swift
//  Water Watch
//
//  Created by Jon on 21/10/2015.
//  Copyright © 2015 Jon McLean. All rights reserved.
//

import Foundation

class JSON {
    
    // Convert String to Dictionary Key:Value pairs to enable labels to be extracted
    func convertStringToDictionary(text: String) -> Array<Dictionary<String, AnyObject>>? {
        //print(text)
        if let data = text.dataUsingEncoding(NSUTF8StringEncoding) {
            do {
                let json = try NSJSONSerialization.JSONObjectWithData(data, options: .MutableContainers) as AnyObject
                let geo = json as? Dictionary<String, AnyObject>
                let dict = geo!["geonames"] as? Array<Dictionary<String, AnyObject>>
                
                return dict
            } catch {
                print("Can not parse JSON return string")
            }
        }
        return nil
    }
    
}