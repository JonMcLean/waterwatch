//
//  API.swift
//  Water Watch
//
//  Created by Jon McLean on 5/11/2015.
//  Copyright © 2015 Jon McLean. All rights reserved.
//

import UIKit

//API written by Ben McLean
class API {
    static func postMessage(title: String, message: String, lat: Double, lng: Double, postID: String, userID: String){
        let titleEncoded: String! = title.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
        let contentEncoded: String! = message.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
        let url = "\(StaticVars.hostName)?request=POST_MESSAGE&key=\(StaticVars.authKey)&title=\(titleEncoded)&message=\(contentEncoded)&lat=\(lat)&lng=\(lng)&post_id=\(postID)&user_id=\(userID)"
        Networking.asyncRequest(url)
    }
    
    enum Sort{
        case hot
        case new
    }
    
    static func getMessages(lat: Double, lng: Double, offset: Int, sort: Sort, radius: Int = 20) -> [Post]{
        var posts:[Post] = []
        
        var url = "\(StaticVars.hostName)?request=GET_MESSAGES&lat=\(lat)&lng=\(lng)&offset=\(offset)&radius=\(radius)"
        
        switch(sort){
        case(Sort.hot):
            url+="&s=h"
        default:
            url+="&s"
        }
        
        var data = Networking.getResult(url) as! [NSMutableDictionary];
        
        for(var i=0; i<data.count; i++){
            let post = Post()
            
            post.userID = data[i]["user_id"] as! String
            post.postID = data[i]["post_id"] as! String
            post.title = data[i]["title"] as! String
            post.content = data[i]["content"] as! String
            post.ups = (data[i]["ups"] as! NSString).integerValue
            post.downs = (data[i]["downs"] as! NSString).integerValue
            post.lat = (data[i]["lat"] as! NSString).doubleValue
            post.lng = (data[i]["lng"] as! NSString).doubleValue
            post.date = data[i]["post_date"] as! String
            
            posts.append(post)
        }
        
        return posts
    }
    
    static func getMessage(postId: String) -> Post{
        let postIdEncoded: String! = postId.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())
        let url = "\(StaticVars.hostName)?request=GET_MESSAGE&key=\(StaticVars.authKey)&id=\(postIdEncoded)"
        
        let data = Networking.getResult(url) as! NSMutableDictionary;
        
        let post = Post()
        post.userID = data["user_id"] as! String
        post.postID = data["post_id"] as! String
        post.title = data["title"] as! String
        post.content = data["content"] as! String
        post.ups = (data["ups"] as! NSString).integerValue
        post.downs = (data["downs"] as! NSString).integerValue
        post.lat = (data["lat"] as! NSString).doubleValue
        post.lng = (data["lng"] as! NSString).doubleValue
        post.date = data["post_date"] as! String
        
        return post
    }
    
    static func upvote(id:String){
        let url = "\(StaticVars.hostName)?request=UPVOTE&post_id=\(id)&key=\(StaticVars.authKey)"
        Networking.asyncRequest(url)
    }
    
    //Deprecated
    static func downvote(id:String){
        let url = "\(StaticVars.hostName)?request=DOWNVOTE&post_id=\(id)&key=\(StaticVars.authKey)"
        Networking.asyncRequest(url)
    }
    
}

//StaticVars written by Ben McLean
class StaticVars {
    //API Authorisation Key
    static let authKey = "caecade6-5ad1-42aa-901b-d66d7570343e"
    //API host
    static let hostName = "http://arctro.com/waterwatch/api/"
    
    //Location history
    static var coords: [[Float!]] = []
}
