//
//  PostDisplayController.swift
//  Water Watch
//
//  Created by Jon on 12/10/2015.
//  Copyright © 2015 Jon McLean. All rights reserved.
//

import UIKit
import CoreLocation

class PostDisplayController: UIViewController, UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate, SidebarDelegate {
    
    // Outlet of UITableView
    @IBOutlet var tableView: UITableView!
    
    // Object of the post temporary storage class. This is used to the later access methods within that class which were not defined as static or class functions.
    var localPost = PostTemporaryStorage()
    
    // Creates object of CLLocationManager.
    var locationManager = CLLocationManager()
    
    // Initialising latitude and longitude as 0.0 for both. This will later be changed to the actual latitude and longitude.
    var latitude: Double = 0.0
    var longitude: Double = 0.0
    
    // Index for sending across to the PostDetailDisplayController class through segue data transfers.
    var index = 0
    
    
    // Object of the Sidebar class so that slide in and out side bar can work within the class.
    var sideBar = Sidebar()
    
    // Creates empty array of Sidebar menu items. This is used to create a global variable of what is found within thw sidebar menu.
    var arrayOfMenuItems: Array<String> = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Loading the post temporary storage when the view loads
        localPost.load()
        
        
        // Setting the sideBar object's delegate to this class.
        sideBar.delegate = self
        
        // Setting the locationManager's delegate to this class.
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyKilometer
        
        locationManager.startUpdatingLocation()
        locationManager.requestAlwaysAuthorization()
        
        let array: Array<String> = ["Posts", "Spots", "Favourites"]
        arrayOfMenuItems = array
        sideBar = Sidebar(sourceView: self.view, menuItems: array)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        sideBar.delegate = self
        
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.loadPosts(0)
        
    }
    
    // Updates the latitude and longitude whenever it changes.
    func locationManager(manager: CLLocationManager, didUpdateToLocation newLocation: CLLocation, fromLocation oldLocation: CLLocation) {
        latitude = Double(newLocation.coordinate.latitude)
        longitude = Double(newLocation.coordinate.longitude)
    }
    
    
    // Sets the number of rows in a section
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return localPost.posts.count
    }
    
    
    // Defines what content will be in each cell as they are being created.
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("PostCell", forIndexPath: indexPath)
        var array = localPost.posts
        var dictionary = array[indexPath.row]
        let name = dictionary["name"] as! String
        let voteCount = dictionary["voteCount"] as! Int
        cell.textLabel!.text = name
        cell.detailTextLabel!.text = "\(String(voteCount)) votes"
        
        return cell
    }
    
    // Will run whenever the cell accessory button is tapped.
    func tableView(tableView: UITableView, accessoryButtonTappedForRowWithIndexPath indexPath: NSIndexPath) {
        index = indexPath.row
        self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
        self.performSegueWithIdentifier("OpenPostDetailDisplay", sender: self)
    }
    
    // Will run whenever the cell in general is tapped.
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        index = indexPath.row
        self.tableView.deselectRowAtIndexPath(indexPath, animated: true)
        self.performSegueWithIdentifier("OpenPostDetailDisplay", sender: self)
    }
    
    // While run when the create page button, found within the navigation controller, is tapped. It will switch the the view to the Post Creation Page.
    @IBAction func addButtonPressed(sender: AnyObject) {
        self.performSegueWithIdentifier("OpenCreatePage", sender: nil)
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "OpenPostDetailDisplay") {
            let vc = segue.destinationViewController as! PostDetailDisplayController
            vc.index = index
        }else if(segue.identifier == "OpenCreatePage") {
            print("Segue \"OpenCreatePage\" has been executed")
        }
    }
    
    // Loads more posts when it reaches the bottom.
    func loadMore() {
        loadPosts(25)
        dispatch_async(dispatch_get_main_queue()) { () -> Void in
            self.tableView.reloadData()
        }
    }
    
    // Will run whenever one of the sideBar button's is tapped.
    func sideBarDidSelectButtonAtIndex(index: Int) {
        if(arrayOfMenuItems[index] == "Posts") {
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("PostNavigationBar") as! UINavigationController
            self.presentViewController(vc, animated: true, completion: nil)
        }else if(arrayOfMenuItems[index] == "Spots") {
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("BeachNavigationBar") as! UINavigationController
            self.presentViewController(vc, animated: true, completion: nil)
        }else if(arrayOfMenuItems[index] == "Favourites") {
            let vc = self.storyboard?.instantiateViewControllerWithIdentifier("FavouritesNavigationBar") as! UINavigationController
            self.presentViewController(vc, animated: true, completion: nil)
        }
        
    }
    
    // Loads certain number of posts (according to offset) and adds them to the temporary post storage class.
    func loadPosts(offset: Int) {
        let posts = API.getMessages(latitude, lng: longitude, offset: offset, sort: API.Sort.hot)
        
        var array: Array<Dictionary<String, AnyObject>> = []
        
        for i in posts {
            let dictionary: Dictionary<String, AnyObject> = ["name": i.title, "voteCount": (i.ups - i.downs), "latitude": i.lat, "longitude": i.lng, "content": i.content, "postID": i.postID, "userID": i.userID]
            array.append(dictionary)
        }
        
        localPost.massAdd(array)
        localPost.save()
    }
    
}
