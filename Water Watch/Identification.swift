
//
//  Identification.swift
//  Water Watch
//
//  Created by Jon on 17/10/2015.
//  Copyright © 2015 Jon McLean. All rights reserved.
//

import Foundation

class Identification {
    
    // Creating object of NSUserDefaults for saving ID to.
    var ud = NSUserDefaults.standardUserDefaults()
    
    // Key which will be used to store and retrive the ID.
    var key = "Water_Watch_Identification"
    
    // Generates, saves, and returns the ID>
    func generateIdentifier() -> String{
        
        // Get the ID as a String from NSUUID
        let ID = NSUUID().UUIDString
        
        // Save it to NSUserDefaults for storing
        ud.setValue(ID, forKey: key)
        
        return ID
    }
    
    // Generates and saves if they key does not already exist.
    func generateIfDoesNotExist() {
        
        // Checks if key exists
        if(ud.valueForKey(key) != nil) { // check out https://github.com/jrendel/SwiftKeychainWrapperExample/blob/master/Pods/SwiftKeychainWrapper/SwiftKeychainWrapper/KeychainWrapper.swift
            print("ID does exist")
        }else {
            
            // Get the ID as a String from NSUUID
            let ID = NSUUID().UUIDString
            
            // Save it to NSUserDefaults for storing
            ud.setValue(ID, forKey: key)
        }
    }
    
    // Get the ID from NSUserDefaults and return it.
    func getIdentifier() -> String{
        
        // Gets the ID from NSUserDefaults and casts it as a String
        let ID = ud.valueForKey(key) as! String
        return ID
    }
    
    // Deletes the ID if the User wants to remove it.
    func deleteIdentifier() {
        
        // Sets a nil value for the inserted key which will therefore remove it from the cache.
        ud.setNilValueForKey(key)
    }
    
}
