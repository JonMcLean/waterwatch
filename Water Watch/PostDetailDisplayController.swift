//
//  PostDetailDisplayController.swift
//  Water Watch
//
//  Created by Jon on 13/10/2015.
//  Copyright © 2015 Jon McLean. All rights reserved.
//

import UIKit
import GoogleMaps
import CoreData

class PostDetailDisplayController: UIViewController, GMSMapViewDelegate {
    
    // Outlets of all the elements required on the View Controller
    @IBOutlet weak var map: GMSMapView!
    @IBOutlet var contentView: UITextView!
    @IBOutlet var upvoteCountLabel: UILabel!
    
    
    var postStorage = PostTemporaryStorage()
    var index: Int = 1
    var latitude = 0.0
    var longitude = 0.0
    var name = ""
    var upvoteCount = 0
    var content = ""
    
    var postID: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        postStorage.load()
        
        var array = postStorage.posts
        var dictionary = array[index]
        latitude = dictionary["latitude"] as! Double
        longitude = dictionary["longitude"] as! Double
        name = dictionary["name"] as! String
        upvoteCount = dictionary["voteCount"] as! Int
        content = dictionary["content"] as! String
        postID = dictionary["postID"] as! String
        
        setup()
    }
    
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        upvoteCountLabel.text = String(upvoteCount)
    }
    
    // Setup the main sections of the view. Including the map and the post content.
    func setup() {
        addMap()
        contentView.text = content
    }
    
    // Adds map to View Controller in order to show where the post originates from
    func addMap() {
        let camera = GMSCameraPosition.cameraWithLatitude(latitude, longitude: longitude, zoom: 6)
        
        map.camera = camera
        map.delegate = self
        
        let marker = GMSMarker()
        marker.position = camera.target //CLLocationCoordinate2DMake(latitude, longitude)
        marker.title = name
        marker.snippet = String(upvoteCount)
        marker.map = map
    }
    
    // Used to send an upvote to the server. Sadly though the server requires 5 minutes before it can return any relevant data due to caching.
    @IBAction func upvoteButtonTapped(sender: AnyObject) {
        upvoteCount++
        upvoteCountLabel.text = String(upvoteCount)
        API.upvote(postID)
    }
    
    // Used to send a downvote to the server. Has the same problem as uploading
    @IBAction func downvoteButtonTapped(sender: AnyObject) {
        upvoteCount--
        upvoteCountLabel.text = String(upvoteCount)
        API.downvote(postID)
    }
    
}
